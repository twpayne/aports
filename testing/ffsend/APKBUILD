# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=ffsend
pkgver=0.2.56
pkgrel=0
pkgdesc=" A fully featured Firefox Send client"
url="https://gitlab.com/timvisee/ffsend"
arch="x86_64 x86 armhf armv7 aarch64 ppc64le" # limited by cargo
license="GPL-3.0-only"
makedepends="cargo openssl-dev"
subpackages="
	$pkgname-zsh-completion:zshcomp:noarch
	$pkgname-fish-completion:fishcomp:noarch
	$pkgname-bash-completion:bashcomp:noarch
	"
source="https://gitlab.com/timvisee/ffsend/-/archive/v$pkgver/ffsend-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"

case "$CARCH" in
	x86)
		export CFLAGS="$CFLAGS -fno-stack-protector"
		;;
esac

build() {
	cargo build --release
}

check() {
	cargo test --release
}

package() {
	cargo install --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates.toml
}

bashcomp() {
	depends=""
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	install -Dm644 "$builddir"/contrib/completions/ffsend.bash \
			"$subpkgdir"/usr/share/bash-completion/completions/ffsend
}

zshcomp() {
	depends=""
	pkgdesc="Zsh compltions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	install -Dm644 "$builddir"/contrib/completions/_ffsend \
			"$subpkgdir"/usr/share/zsh/site-functions/_ffsend
}

fishcomp() {
	depends=""
	pkgdesc="Fish completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel fish"

	install -Dm644 "$builddir"/contrib/completions/ffsend.fish \
			"$subpkgdir"/usr/share/fish/completions/ffsend.fish
}

sha512sums="7b4e2bc424a4483f5095cb50c8e85f8cecef99135f2a30272b584cb8c36d2304d98f982ef057f6ba12f07c6aa7c014562a175ee3c3280676909a478729b3e17b  ffsend-v0.2.56.tar.gz"
